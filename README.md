# rbx-vhdx

### usage

    const vhdx = require('rbx-vhdx');

    vhdx.info('http://example.com/test.vhdx', console.log);
    vhdx.info('C:\\Users\\rbx\\disk.vhdx', console.log);

    {
        type: 'dynamic'     //dynamic || fixed || differencing
      , size: 1073741824    //bytes
    }
